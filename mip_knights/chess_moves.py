import itertools
from typing import Tuple, List, Iterator


def generate_constrained_moves(
    all_moves: List[Tuple[int, int]], allowed_move_sizes: int
):
    # 'allowed_move_sizes' is a slightly rubbish name.  It's actually the set of
    # L1-norms which the piece moves have to have.  See also - the taxicab norm.
    return [(i, j) for i, j in all_moves if abs(i) + abs(j) in allowed_move_sizes]


def generate_knight_moves() -> List[Tuple[int, int]]:
    # Move around like a chess knight
    knight_axis_steps = (2, -2, 1, -1)
    return generate_constrained_moves(
        itertools.product(knight_axis_steps, knight_axis_steps),
        {3},
    )


def generate_wasd_moves() -> List[Tuple[int, int]]:
    # Move around like a 'wasd' video game character
    simple_axis_steps = (0, 1, -1)
    return generate_constrained_moves(
        itertools.product(simple_axis_steps, simple_axis_steps),
        {1},
    )


def linearly_scale_allowed_moves(
    allowed_moves: List[Tuple[int, int]], N: int
) -> List[Tuple[int, int]]:
    return [(n * i, n * j) for (i, j) in allowed_moves for n in range(N)]


def generate_bishop_moves(N: int) -> List[Tuple[int, int]]:
    # Move around like a bishop
    simple_axis_steps = (1, -1)
    base_moves = generate_constrained_moves(
        itertools.product(simple_axis_steps, simple_axis_steps),
        {2},
    )
    return linearly_scale_allowed_moves(base_moves, N)


def generate_queen_moves(N: int) -> List[Tuple[int, int]]:
    # Move around like a chess queen
    simple_axis_steps = (0, 1, -1)
    base_moves = generate_constrained_moves(
        itertools.product(simple_axis_steps, simple_axis_steps),
        {1, 2},
    )
    return linearly_scale_allowed_moves(base_moves, N)


def is_coordinate_valid(i: int, j: int, N: int) -> bool:
    return i < N and j < N and i >= 0 and j >= 0


def legal_moves(
    i: int, j: int, moves: List[Tuple[int, int]], N: int
) -> Iterator[Tuple[int, int]]:
    for move_x, move_y in moves:
        new_i, new_j = i + move_x, j + move_y
        if is_coordinate_valid(new_i, new_j, N):
            yield new_i, new_j
