from typing import List, Set
import time
import chess
import chess.svg
import glob
import cairosvg
import tempfile
from PIL import Image


def write_svg_to_file(filename: str, board: chess.svg.SvgWrapper) -> None:
    with open(filename, "w") as f:
        f.write(board._repr_svg_())


def convert_board_images_to_png(image_dir: str) -> None:
    for image in sorted(glob.glob(f"{image_dir}/*.svg")):
        cairosvg.svg2png(url=image, write_to=image.replace(".svg", ".png"))


def make_gif(image_dir: str, prefix: str = "knight_problem_solution") -> None:
    frames = [Image.open(image) for image in sorted(glob.glob(f"{image_dir}/*.png"))]
    frames[0].save(
        f"./{prefix}_{int(time.time())}.gif",
        format="GIF",
        append_images=frames,
        save_all=True,
        duration=250,
        loop=1,
    )


def board_to_svg(board: chess.Board, ticked_squares: Set[chess.Square] = {}) -> str:
    queen_positions = [
        k for (k, v) in board.piece_map().items() if v.piece_type == chess.QUEEN
    ]
    filled_squares = {}
    # Mark the squares attacked by the queen in red
    for queen_position in queen_positions:
        filled_squares.update(dict.fromkeys(board.attacks(queen_position), "#cc0000cc"))
    # Mark the 'ticked' squares in green
    for ticked in ticked_squares:
        filled_squares[ticked] = "#00cc00cc"
    return chess.svg.board(
        board,
        fill=filled_squares,
        size=350,
    )


def write_boards_to_svg(
    image_dir: str,
    boards: List[chess.Board],
    ticked_squares: List[Set[chess.Square]] = None,
) -> None:
    ticked_squares = (
        ticked_squares
        if ticked_squares is not None
        else [set() for _ in range(len(boards))]
    )
    for t, (board, ticks) in enumerate(zip(boards, ticked_squares)):
        write_svg_to_file(
            f"{image_dir}/board_{str(t).zfill(5)}.svg", board_to_svg(board, ticks)
        )


def make_solution_gif(
    boards: List[chess.Board], ticked_squares: List[Set[chess.Square]], prefix: str
) -> None:
    with tempfile.TemporaryDirectory() as tmp_dirname:
        write_boards_to_svg(tmp_dirname, boards, ticked_squares)
        convert_board_images_to_png(tmp_dirname)
        make_gif(tmp_dirname, prefix)
