from typing import Tuple, List, Optional, Set
from enum import Enum
from dataclasses import dataclass, astuple
import numpy as np
import gurobipy as lp
import chess
import logging
from functools import wraps
from .utils import (
    pychess_to_cartesian,
    generate_attacked_squares,
    build_problem_board,
    get_numpy_boards,
    get_pychess_boards,
)
from .display import make_solution_gif
from .chess_moves import generate_knight_moves, legal_moves


logging.basicConfig(level=logging.INFO)


class ModelError(Exception):
    pass


class ObjectiveChoice(Enum):
    # We just want a feasible solution then we have a constant objective function
    # and any configuration of knight moves which works will do
    FIND_FEASIBLE_SOLUTION = 1

    # Here we can use Gurobi's nice lexicographical objectives to first minimise
    # the number of time steps used, and then once that is optimal try to minimise
    # the number of knight moves.  If we optimised for time steps directly, alone,
    # the optimisation has no reason to remove knights from the board early even
    # if those knights are not making useful moves
    MINIMISE_TIME_STEPS = 2

    # Minimise the number of knight moves, regardless of how many time-steps they
    # are taken over
    MINIMISE_KNIGHT_MOVES = 3


@dataclass
class ProblemDimensions:
    """
    Attributes:
        N: The size of the chess board, which will be a grid with dimension (N, N)
        T: The time dimension of the problem, each knight lives in a cube with dimension (N, N, T)
        K: The number of knight pieces playing in a particular problem
    """

    N: int
    T: int
    K: int

    def __iter__(self):
        return iter(astuple(self))


@dataclass
class GurobiVars:
    """
    Attributes:
        X: an array with dimension (N, N, T, K).  X[i, j, t, k] == 1 -> the piece k, is at square (i, j) at time t
        Y: an array with dimension (N, N).  Y[i, j] == 1 -> one of the knights has visited square (i, j)
        Z: an array with dimension (T,).  Z[t] == 1 -> at least one knight move was made at time t

        M: a scalar which counts of the number of knight moves used to complete the problem

        S: a scalar which counts of the number of time-steps used to complete the problem,
           When only one knight is playing `_S == _M`.
    """

    X: lp.Var
    Y: lp.Var
    Z: lp.Var
    M: lp.Var
    S: lp.Var


# Some methods should only be run once the optimisation problem has
# been solved to optimality, use this to protect those methods
def requies_optimality(inner_method):
    @wraps(inner_method)
    def wrapper(self, *args, **kwargs):
        if self._model.status != lp.GRB.OPTIMAL:
            raise RuntimeError("KnightModel is not solved to optimality!")
        return inner_method(self, *args, **kwargs)

    return wrapper


DEFAULT_BOARD_SIZE = 8
DEFAULT_TIME_STEPS = DEFAULT_BOARD_SIZE * DEFAULT_BOARD_SIZE


class KnightProblem:
    """
    Given the locations of some queens and some knights, find a (possibly
    optimal) path for each knight to take around the board, avoiding the
    attack of the queen(s), such that each of the objective_squares is touched
    at least once
    """

    def __init__(
        self,
        initial_knight_positions: List[chess.Square],
        queen_positions: List[chess.Square] = [],
        *,
        board_size: int = DEFAULT_BOARD_SIZE,
        maximum_time_steps: int = DEFAULT_TIME_STEPS,
        objective_choice: ObjectiveChoice = ObjectiveChoice.FIND_FEASIBLE_SOLUTION,
        move_set: Optional[List[Tuple[int, int]]] = None,
        objective_squares: Optional[Set[chess.Square]] = None,
    ):
        self._knight_moves = (
            move_set if move_set is not None else generate_knight_moves()
        )

        # Determines the objective function to be used
        self._objective_choice = objective_choice

        # The dimensions of the problem
        self._dimensions = ProblemDimensions(
            board_size,
            maximum_time_steps,
            len(initial_knight_positions),
        )

        # The chess library actually just uses ints under the
        # hood so a range over ints is fine here
        self._all_squares = set(s for s in range(self._dimensions.N**2))

        self.board = build_problem_board(initial_knight_positions, queen_positions)
        self._queen_positions = queen_positions
        self._initial_knight_positions = initial_knight_positions

        # Compute the set of squares which are attacked, pruning
        # down to only include ones which are active for the users choice of 'N'
        self._attacked_squares = set(
            [
                attacked
                for attacked in generate_attacked_squares(self.board, queen_positions)
                if attacked in self._all_squares
            ]
        )

        # The set of squares we have to reach for the problem to be complete
        # remembering to remove the attacked squares
        objective_squares = (
            objective_squares if objective_squares is not None else self._all_squares
        )
        self._objective_squares = set(
            [
                s
                for s in objective_squares
                if s in self._all_squares and s not in self._attacked_squares
            ]
        )

        # Placeholders for Gurobi state
        self._model: lp.Model = None
        self._gurobi_vars: GurobiVars = None

        self._check_problem_input()
        self._setup_gurobi_model()

    def _check_problem_input(self) -> None:
        # Check that the knights aren't attacked at t=0
        if set(self._attacked_squares) & set(self._initial_knight_positions):
            raise ModelError("Initial board setup has knights attacked")

    def _setup_gurobi_model(self) -> None:
        self._model = lp.Model()

        # Disable gurobi model logging because its very chattty
        self._model.setParam("OutputFlag", 0)

        self._add_gurobi_variables()
        self._add_gurobi_constraints()
        self._add_gurobi_objective()

    def _add_gurobi_variables(self) -> None:
        # Slightly more efficient and less ugly code...
        N, T, K = self._dimensions

        self._gurobi_vars = GurobiVars(
            self._model.addVars(N, N, T, K, vtype=lp.GRB.BINARY, name="x"),
            self._model.addVars(N, N, vtype=lp.GRB.BINARY, name="y"),
            self._model.addVars(T, vtype=lp.GRB.BINARY, name="z"),
            self._model.addVar(vtype=lp.GRB.INTEGER, name="m"),
            self._model.addVar(vtype=lp.GRB.INTEGER, name="s"),
        )

    def _add_gurobi_constraints(self) -> None:
        N, T, K = self._dimensions

        # Initial condition for each knight
        self._model.addConstrs(
            (
                self._gurobi_vars.X[
                    (*pychess_to_cartesian(self._initial_knight_positions[k], N), 0, k)
                ]
                == 1
                for k in range(K)
            ),
            name="knights_ICs",
        )

        # For every knight, for every time step, constrain that the piece cannot live
        # on an attacked square
        self._model.addConstrs(
            (
                self._gurobi_vars.X[(*pychess_to_cartesian(s, N), t, k)] == 0
                for s in self._attacked_squares
                for k in range(K)
                for t in range(T)
            ),
            name="knights_cant_be_attacked",
        )

        # Each knight can be in *at most* one place on the board at each point in time.
        # This is not a strict equality because the model can remove knights from the
        # board when they are no longer needed (e.g. when all the squares have been
        # visited by a knight)
        self._model.addConstrs(
            (
                lp.quicksum(
                    self._gurobi_vars.X[i, j, t, k] for i in range(N) for j in range(N)
                )
                <= 1
                for t in range(T)
                for k in range(K)
            ),
            name="knights_one_square_at_a_time_each",
        )

        # No two knights can share a square at any point in time
        self._model.addConstrs(
            (
                lp.quicksum(self._gurobi_vars.X[i, j, t, k] for k in range(K)) <= 1
                for i in range(N)
                for j in range(N)
                for t in range(T)
            ),
            name="one_knight_per_square",
        )

        # Constrain the piece to only take legal moves:  This is effectively
        # 'conservation of knights'.  We're saying that at time=t knight=k can
        # only be at (i, j) if if was one knight-move away from (i, j) at time=t-1
        self._model.addConstrs(
            (
                self._gurobi_vars.X[i, j, t, k]
                <= lp.quicksum(
                    self._gurobi_vars.X[move_x, move_y, t - 1, k]
                    for move_x, move_y in legal_moves(i, j, self._knight_moves, N)
                )
                for i in range(N)
                for j in range(N)
                for t in range(1, T)
                for k in range(K)
            ),
            name="legal_moves",
        )

        # If any knight is on the board at time=t then a move has been made and so we
        # need to switch Z[t] 'on' in order to count the number of time-steps used to
        # complete the problem
        self._model.addConstrs(
            (
                (
                    lp.quicksum(
                        self._gurobi_vars.X[i, j, t, k]
                        for i in range(N)
                        for j in range(N)
                    )
                    <= self._gurobi_vars.Z[t]
                )
                for t in range(T)
                for k in range(K)
            ),
            name="timestep_tracking_1",
        )

        # 'S' tracks the total number of moves made, which is just the sum of 'Z'
        self._model.addConstr(
            lp.quicksum(self._gurobi_vars.Z) == self._gurobi_vars.S,
            name="timestep_tracking_2",
        )

        # Y can only be 'switched on' for square (i, j) if we've visited that square with a knight
        self._model.addConstrs(
            (
                self._gurobi_vars.Y[i, j]
                <= lp.quicksum(
                    self._gurobi_vars.X[i, j, t, k] for t in range(T) for k in range(K)
                )
                for i in range(N)
                for j in range(N)
            ),
            name="completion_criteria_1",
        )

        # We have to visit every un-attacked square to complete the game
        self._model.addConstr(
            lp.quicksum(
                self._gurobi_vars.Y[pychess_to_cartesian(s, N)]
                for s in self._objective_squares
            )
            >= len(self._objective_squares),
            name="completion_criteria_2",
        )

        # Count the times the knights appear to track how many knight moves were made,
        # we have to correct with 'K' since the knights appear at time=0 but no moves
        # were made in that step
        self._model.addConstr(
            self._gurobi_vars.M
            == lp.quicksum(
                self._gurobi_vars.X[i, j, t, k]
                for i in range(N)
                for j in range(N)
                for t in range(T)
                for k in range(K)
            )
            - K,
            name="completion_criteria_2",
        )

    def _add_gurobi_objective(self) -> None:
        if self._objective_choice == ObjectiveChoice.FIND_FEASIBLE_SOLUTION:
            pass

        elif self._objective_choice == ObjectiveChoice.MINIMISE_TIME_STEPS:
            self._model.setObjectiveN(self._gurobi_vars.S, 0, 2)
            self._model.setObjectiveN(self._gurobi_vars.M, 1, 1)

        elif self._objective_choice == ObjectiveChoice.MINIMISE_KNIGHT_MOVES:
            self._model.setObjective(self._gurobi_vars.M)

        else:
            raise RuntimeError(f"Unknown ObjectiveChoice! {self._objective_choice}")

    def solve(self) -> None:
        self._model.optimize()
        if self._model.status != lp.GRB.OPTIMAL:
            raise ModelError(
                f"KnightModel is unfeasible or unbounded! {self._model.status}"
            )

    @requies_optimality
    def get_numpy_boards(self) -> np.array:
        return get_numpy_boards(self._gurobi_vars.X, *self._dimensions)

    @requies_optimality
    def get_pychess_boards(self) -> List[chess.Board]:
        return get_pychess_boards(
            self.get_numpy_boards(), self._queen_positions, self._dimensions.T
        )

    @requies_optimality
    def get_number_of_moves(self) -> int:
        return int(self._gurobi_vars.M.X)

    @requies_optimality
    def get_number_of_time_steps(self) -> int:
        return int(self._gurobi_vars.S.X)

    @requies_optimality
    def get_ticked_squares(self) -> List[Set[chess.Square]]:
        # This is a noop for now but in principle it requires a
        # solution to know when to 'tick' squares
        return None

    def get_gif_prefix(self) -> str:
        return "knight_problem"

    @requies_optimality
    def make_solution_gif(self) -> None:
        make_solution_gif(
            self.get_pychess_boards(), self.get_ticked_squares(), self.get_gif_prefix()
        )
