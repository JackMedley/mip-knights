import chess
import numpy as np
import gurobipy as lp
from typing import Tuple, List, Dict


def pychess_to_cartesian(chess_square: chess.Square, N: int = 8) -> Tuple[int, int]:
    return int(chess_square) % N, int(chess_square) // N


def cartesian_to_pychess(i: int, j: int, N: int = 8) -> chess.Square:
    return i + N * j


def build_problem_board(
    knight_positions: List[chess.Square],
    queen_positions: List[chess.Square],
) -> chess.Board:
    board = chess.Board()
    K = chess.Piece(chess.KNIGHT, True)
    Q = chess.Piece(chess.QUEEN, False)
    positions = {p: K for p in knight_positions}
    positions.update({p: Q for p in queen_positions})
    board.set_piece_map(positions)
    return board


def get_numpy_boards(X: lp.Var, N: int, T: int, K: int) -> np.array:
    return np.fix(
        np.array(
            [
                X[i, j, t, k].X
                for i in range(N)
                for j in range(N)
                for t in range(T)
                for k in range(K)
            ]
        )
        .reshape(N, N, T, K)
        .sum(axis=-1)
    ).astype(int)


def get_cartesian_knight_locations(
    numpy_boards: np.array, T: int
) -> Dict[int, List[chess.Square]]:
    return {
        t: [
            cartesian_to_pychess(a, b)
            for (a, b) in np.array(np.where(numpy_boards[:, :, t] == 1)).T
        ]
        for t in range(T)
    }


def get_pychess_boards(
    numpy_boards: np.array, queen_positions, T: int
) -> List[chess.Board]:
    cartesian_knight_positions = get_cartesian_knight_locations(numpy_boards, T)
    return [
        build_problem_board(cartesian_knight_positions[t], queen_positions)
        for t in range(T)
    ]


def generate_attacked_squares(
    board: chess.Board,
    queen_positions: List[chess.Square],
) -> List[chess.Square]:
    # Find the squares under attack from each of the queens
    attacked = [cs for qp in queen_positions for cs in board.attacks(qp)]

    # Include the locations of the queens themselves
    attacked += [qp for qp in queen_positions]

    # Don't forget to remove duplicate entries since we use the length of this set
    return set(attacked)
