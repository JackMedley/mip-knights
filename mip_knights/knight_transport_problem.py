from typing import List
import chess
from .knight_problem import KnightProblem, ModelError


class KnightTransportProblem(KnightProblem):
    """
    Given an initial, and desired final, location for a single knight find the
    (possibly optimal) path to get the knight from A to B while avoiding the attack
    of the queen(s)
    """

    def __init__(
        self,
        initial_knight_position: chess.Square,
        final_knight_position: chess.Square,
        queen_positions: List[chess.Square] = [],
        **kwargs,
    ):
        if "objective_squares" in kwargs:
            raise RuntimeError(
                "objective_squares is implied by the final knight position in a transport problem"
            )

        # Translate the final knight position into an objective_squares set with a single entry
        # and then call the parent constructor
        self._final_knight_position = final_knight_position
        super().__init__(
            [initial_knight_position],
            queen_positions,
            objective_squares=set([final_knight_position]),
            **kwargs,
        )

    def _check_problem_input(self) -> None:
        # We don't need a MIP problem to solve these problems
        if self._initial_knight_positions[0] == self._final_knight_position:
            raise ModelError(
                "Knight transport trivial when initial_position == final_positions"
            )

        # Check that the knight's final position isn't attacked
        if self._final_knight_position in set(self._attacked_squares):
            raise ModelError("Knight final position is attacked")

    def get_gif_prefix(self) -> str:
        return "knight_transport_problem"
