from typing import List, Set
import chess
import numpy as np
from .knight_transport_problem import KnightTransportProblem
from .knight_problem import ObjectiveChoice
from .utils import build_problem_board, generate_attacked_squares, get_pychess_boards
from .display import make_solution_gif


class FinegoldProblem:
    """
    Break the 'Finegold' problem into a series of knight transport problems
    and solve to optimality
    """

    def __init__(self, knight_positions=None, queen_positions=None):
        self._knight_positions = (
            [chess.H8] if knight_positions is None else knight_positions
        )
        self._queen_positions = (
            [chess.D5] if queen_positions is None else queen_positions
        )

        self.board = build_problem_board(self._knight_positions, self._queen_positions)
        self._all_squares = list(
            reversed(
                [
                    s
                    for s in range(64)
                    if s
                    not in generate_attacked_squares(self.board, self._queen_positions)
                ]
            )
        )
        self._number_of_subproblems = len(self._all_squares) - 1

        self._numpy_boards = []
        self._number_of_moves = []

    def solve(self):
        for initial_knight_position, final_knight_position in zip(
            self._all_squares[:-1], self._all_squares[1:]
        ):
            knight_problem = KnightTransportProblem(
                initial_knight_position,
                final_knight_position,
                self._queen_positions,
                objective_choice=ObjectiveChoice.MINIMISE_TIME_STEPS,
            )

            knight_problem.solve()
            moves = knight_problem.get_number_of_moves()
            self._number_of_moves.append(moves)

            numpy_boards = knight_problem.get_numpy_boards()
            start_idx = 0 if initial_knight_position == self._knight_positions[0] else 1
            numpy_boards = numpy_boards[:, :, start_idx : moves + 1]

            self._numpy_boards.append(numpy_boards)

    def get_numpy_boards(self) -> np.array:
        return np.dstack(self._numpy_boards)

    def get_pychess_boards(self) -> List[chess.Board]:
        return get_pychess_boards(
            self.get_numpy_boards(),
            self._queen_positions,
            self.get_number_of_time_steps() + 1,
        )

    def get_number_of_moves(self):
        return self.get_number_of_time_steps()

    def get_number_of_time_steps(self):
        return sum(self._number_of_moves)

    def get_gif_prefix(self) -> str:
        return "finegold_problem"

    def get_ticked_squares(self) -> List[Set[chess.Square]]:
        # For the Finegold problem each square gets ticked at the end of each subproblem
        # (that is the completion criteria for the subproblem)
        ticked_squares = []

        for subproblem_idx in range(self._number_of_subproblems):
            # Find the squares which were ticked at the start of this subproblem
            squares_ticked_at_start = set(self._all_squares[: subproblem_idx + 1])

            # We need 'moves - 1' copies where this subproblem remains uncompleted,
            for _ in range(self._number_of_moves[subproblem_idx]):
                ticked_squares.append(squares_ticked_at_start)

        # And one final one with the entire board complete
        ticked_squares.append(set(self._all_squares))

        return ticked_squares

    def make_solution_gif(self) -> None:
        make_solution_gif(
            self.get_pychess_boards(), self.get_ticked_squares(), self.get_gif_prefix()
        )
