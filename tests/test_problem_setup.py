import pytest
import chess
from mip_knights.chess_moves import legal_moves
from mip_knights.knight_problem import (
    DEFAULT_BOARD_SIZE,
    KnightProblem,
    ModelError,
)
from mip_knights.knight_transport_problem import KnightTransportProblem
from mip_knights.utils import pychess_to_cartesian, cartesian_to_pychess


def test_generate_wasd(wasd_moves):
    assert len(wasd_moves) == 4
    assert set(wasd_moves) == set([(0, 1), (0, -1), (1, 0), (-1, 0)])


@pytest.mark.parametrize(
    "i, j, N, legal_count",
    [
        (0, 0, 3, 2),
        (0, 1, 3, 3),
        (1, 0, 3, 3),
        (1, 0, 2, 2),
        (1, 1, 3, 4),
    ],
)
def test_legal_moves_wasd(wasd_moves, i, j, N, legal_count):
    assert len(list(legal_moves(i, j, wasd_moves, N))) == legal_count


@pytest.mark.parametrize(
    "i, j, legal_count",
    [
        (0, 0, 2),
        (1, 1, 4),
        (2, 2, 8),
    ],
)
def test_legal_moves_knight(knight_moves, i, j, legal_count):
    assert len(list(legal_moves(i, j, knight_moves, DEFAULT_BOARD_SIZE))) == legal_count


@pytest.mark.parametrize("pychess_coordinate", range(64))
def test_coordinate_systems_1(pychess_coordinate):
    assert pychess_coordinate == cartesian_to_pychess(
        *pychess_to_cartesian(pychess_coordinate, DEFAULT_BOARD_SIZE),
        DEFAULT_BOARD_SIZE
    )


@pytest.mark.parametrize(
    "cartesian_coordinate",
    [(i, j) for i in range(DEFAULT_BOARD_SIZE) for j in range(DEFAULT_BOARD_SIZE)],
)
def test_coordinate_systems_2(cartesian_coordinate):
    assert cartesian_coordinate == pychess_to_cartesian(
        cartesian_to_pychess(*cartesian_coordinate, DEFAULT_BOARD_SIZE),
        DEFAULT_BOARD_SIZE,
    )


@pytest.mark.parametrize(
    "queen_position, is_valid",
    [
        (chess.D5, True),
        (chess.D6, False),
    ],
)
def test_bad_initial_positions(queen_position, is_valid):
    problem_builder = lambda: KnightProblem(
        [chess.H6, chess.H7, chess.H8], [queen_position]
    )

    if is_valid:
        # Won't throw
        problem_builder()
    else:
        with pytest.raises(ModelError):
            # Should throw!
            problem_builder()


@pytest.mark.parametrize(
    "knight_initial_position, knight_final_position, is_valid",
    [
        (chess.H8, chess.F8, True),
        (chess.H8, chess.G8, False),
        (chess.H8, chess.H8, False),
    ],
)
def test_bad_final_knight_position(
    knight_initial_position, knight_final_position, is_valid
):
    problem_builder = lambda: KnightTransportProblem(
        knight_initial_position, knight_final_position, [chess.D5]
    )

    if is_valid:
        # Won't throw
        problem_builder()
    else:
        with pytest.raises(ModelError):
            # Should throw!
            problem_builder()
