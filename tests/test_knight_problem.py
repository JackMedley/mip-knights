import pytest
import chess
from mip_knights.knight_problem import (
    KnightProblem,
    ModelError,
    ObjectiveChoice,
    DEFAULT_BOARD_SIZE,
    DEFAULT_TIME_STEPS,
)


def check_feasibility(
    board_size,
    maximum_time_steps,
    move_set,
    is_feasible,
    objective_choice=None,
    knight_positions=None,
    queen_positions=None,
):
    try:
        KnightProblem(
            knight_positions if knight_positions else [chess.A1],
            queen_positions if queen_positions else [],
            board_size=board_size,
            maximum_time_steps=maximum_time_steps,
            move_set=move_set,
            objective_choice=objective_choice or ObjectiveChoice.FIND_FEASIBLE_SOLUTION,
        ).solve()
    except ModelError:
        if is_feasible:
            raise AssertionError("Model is infeasible when it shouldnt be")
    else:
        if not is_feasible:
            raise AssertionError("Model is feasible when it shouldnt be")


@pytest.mark.parametrize(
    "board_size, maximum_time_steps, is_feasible",
    [
        (2, 4, True),
        (4, 4, False),
        (2, 3, False),
    ],
)
def test_wasd_feasibility(board_size, maximum_time_steps, wasd_moves, is_feasible):
    check_feasibility(board_size, maximum_time_steps, wasd_moves, is_feasible)


@pytest.mark.parametrize(
    "board_size, maximum_time_steps, is_feasible",
    [
        (2, 20, False),  # Not enough room to manoeuvre
        (3, 20, False),  # Not enough room to manoeuvre
        (4, 20, True),
        (4, 16, False),  # Not enough time to get around the board
    ],
)
def test_knight_feasibility(board_size, maximum_time_steps, knight_moves, is_feasible):
    check_feasibility(board_size, maximum_time_steps, knight_moves, is_feasible)


def test_bad_objective_choice(wasd_moves):
    checker = lambda ojc: check_feasibility(
        2, 4, wasd_moves, True, objective_choice=ojc
    )

    # Should be fine
    checker(ObjectiveChoice.FIND_FEASIBLE_SOLUTION)

    # Should rightly explode
    with pytest.raises(RuntimeError):
        checker("ObjectiveChoice.FIND_FEASIBLE_SOLUTION")


def test_requires_optimality():
    kp = KnightProblem([chess.A1], board_size=4)

    with pytest.raises(RuntimeError):
        kp.get_numpy_boards()

    # Solve it first, then we should be good to go
    kp.solve()
    kp.get_numpy_boards()


@pytest.mark.parametrize(
    "knight_positions, queen_positions, is_feasible",
    [
        # Perfectly reasonable
        ([chess.F8, chess.H6], [chess.A1], True),
        # The 'WASD knight' is trapped by the queen's diagonal
        ([chess.H6], [chess.A1], False),
    ],
)
def test_wasd_infeasible_board(
    wasd_moves, knight_positions, queen_positions, is_feasible
):
    check_feasibility(
        DEFAULT_BOARD_SIZE,
        DEFAULT_TIME_STEPS,
        wasd_moves,
        is_feasible,
        knight_positions=knight_positions,
        queen_positions=queen_positions,
    )


@pytest.mark.parametrize(
    "knight_positions, queen_positions, is_feasible",
    [
        # Perfectly reasonable
        ([chess.F8, chess.H6], [chess.A1], True),
        # The knight can jump over the queen's diagonal
        ([chess.H6], [chess.A1], True),
        # ...it can just about jump over two consecutive diagnoals
        ([chess.H6], [chess.A1, chess.A2], True),
        # ...but it can't jump over three!
        ([chess.H6], [chess.A1, chess.A2, chess.A3], False),
    ],
)
def test_knight_infeasible_board(
    knight_moves, knight_positions, queen_positions, is_feasible
):
    check_feasibility(
        DEFAULT_BOARD_SIZE,
        DEFAULT_TIME_STEPS,
        knight_moves,
        is_feasible,
        knight_positions=knight_positions,
        queen_positions=queen_positions,
    )


@pytest.mark.parametrize(
    "knight_squares, board_size, expected_number_of_moves, expected_number_of_time_steps",
    [
        # Put knights on every square and check that the problem
        # is recognised as trivially solved immediately
        (list(range(4)), 2, 0, 1),
        (list(range(9)), 3, 0, 1),
        # Put knights on every square, except one, are check that the problem
        # is recognised as trivially solved by making a single move
        (list(range(4 - 1)), 2, 1, 2),
        (list(range(9 - 1)), 3, 1, 2),
    ],
)
def test_trivial_boards(
    wasd_moves,
    knight_squares,
    board_size,
    expected_number_of_moves,
    expected_number_of_time_steps,
):
    kp = KnightProblem(
        knight_squares,
        move_set=wasd_moves,
        board_size=board_size,
        objective_choice=ObjectiveChoice.MINIMISE_TIME_STEPS,
    )
    kp.solve()
    assert kp.get_number_of_moves() == expected_number_of_moves
    assert kp.get_number_of_time_steps() == expected_number_of_time_steps
