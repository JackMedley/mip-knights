import pytest
from mip_knights.chess_moves import generate_wasd_moves, generate_knight_moves


@pytest.fixture(scope="session")
def wasd_moves():
    yield generate_wasd_moves()


@pytest.fixture(scope="session")
def knight_moves():
    yield generate_knight_moves()
