# MIP Knights

Solving the knights vs. queen problem [1] using mixed integer programming.

There are examples of how to solve problems in the `tests` directory (you will need a Gurobi license, at the time of writing they include a limited-use license when you install `gurobipy` from pip.  If you're a student or an academic you can get one for free from their website).

I'll include the specific problem setup required to solve [1] and an optimal solution to it (I haven't checked if it is unique, though it would be easy to do so using Gurobi's solution pool feature):

```python
from mip_knights.finegold_problem import FinegoldProblem
problem = FinegoldProblem()
problem.solve()
problem.get_number_of_moves()
>> 158
```

![image info](./finegold_problem_1653653344.gif)

If you have alternative ways of solving this problem (especially ways which can find better solutions!) feel free to email me at jackjmedley@gmail.com.

Thanks to Mark Harley for his code review!

[1] https://plasmatech8.github.io/KnightAndQueenPuzzle/
